package br.com.passaradiante.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.com.passaradiante.model.UsuarioCadastro;
import br.com.passaradiante.services.UsuarioCadastroServices;

@RestController
@RequestMapping("/api/usuarioCadastro")
public class UsuarioCadastroController {
	
	@Autowired
	private UsuarioCadastroServices services;
	
	
	@GetMapping
	public List<UsuarioCadastro> findAll() {
		return services.listarTodos();
	}
	
	@GetMapping(value="/encontrarPorId/{id}")
	public UsuarioCadastro findById(@PathVariable("id") Long id) {
		return services.encontrarPorId(id);
	}
	
	@PutMapping(value="/atualizar")
	public UsuarioCadastro update(@RequestBody UsuarioCadastro usuariocadastro) {
		return services.atualizarCadastro(usuariocadastro);
	}
	
	@PostMapping(value="/adicionar")
	public UsuarioCadastro create(@RequestBody UsuarioCadastro usuariocadastro) {
		return services.adicionar(usuariocadastro);
	}
	
	@DeleteMapping(value="/deletarPorId/{id}")
	public void delete(@PathVariable("id") Long id) {
		services.delete(id);
	}
	
	
}
