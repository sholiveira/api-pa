package br.com.passaradiante.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.com.passaradiante.model.Produto;
import br.com.passaradiante.services.ProdutoServices;

@RestController
@RequestMapping("/api/produto")
public class ProdutoController {
	
	@Autowired
	private ProdutoServices services;
	
	
	@GetMapping
	public List<Produto> findAll() {
		return services.listarTodos();
	}
	
	@GetMapping(value="/encontrarPorId/{id}")
	public Produto findById(@PathVariable("id") Long id) {
		return services.encontrarPorId(id);
	}
	
	@PutMapping(value="/atualizar")
	public Produto update(@RequestBody Produto produto) {
		return services.atualizarCadastro(produto);
	}
	
	@PostMapping(value="/adicionar")
	public Produto create(@RequestBody Produto produto) {
		return services.adicionar(produto);
	}
	
	@DeleteMapping(value="/deletarPorId/{id}")
	public void delete(@PathVariable("id") Long id) {
		services.delete(id);
	}
	
	
}
