package br.com.passaradiante.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import br.com.passaradiante.model.UsuarioCadastro;

@Repository
public interface UsuarioCadastroRepository extends JpaRepository<UsuarioCadastro, Long> {

}
