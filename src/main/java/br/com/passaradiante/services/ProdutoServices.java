package br.com.passaradiante.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.com.passaradiante.exception.ResourceNotfoundException;
import br.com.passaradiante.model.Produto;
import br.com.passaradiante.repository.ProdutoRepository;
import br.com.passaradiante.repository.UsuarioCadastroRepository;

@Service
public class ProdutoServices {
	
	@Autowired
	ProdutoRepository repository;
	
	public Produto adicionar(Produto produto) {
		return repository.save(produto);
	}
	
	public List<Produto> listarTodos() {
		return repository.findAll();
	}
	
	public Produto encontrarPorId(Long id) {
		return repository.findById(id)
		.orElseThrow(() -> new ResourceNotfoundException("Sem registro para esse ID."));
	}
	
	public Produto atualizarCadastro(Produto p) {
		Produto entity = repository.findById(p.getId())
				.orElseThrow(() -> new ResourceNotfoundException("Sem registro para esse ID."));
		entity.setNome(p.getNome());
		entity.setDescricao(p.getDescricao());
		entity.setQuantidade(p.getQuantidade());
		entity.setValor(p.getValor());
		return repository.save(entity);
	}

	public void delete(Long id) {
		Produto entity = repository.findById(id)
				.orElseThrow(() -> new ResourceNotfoundException("Sem registro para esse ID."));
		repository.delete(entity);
	}
	
}
