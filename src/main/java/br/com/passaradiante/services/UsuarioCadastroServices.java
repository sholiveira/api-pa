package br.com.passaradiante.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.com.passaradiante.exception.ResourceNotfoundException;
import br.com.passaradiante.model.UsuarioCadastro;
import br.com.passaradiante.repository.UsuarioCadastroRepository;

@Service
public class UsuarioCadastroServices {
	
	@Autowired
	UsuarioCadastroRepository repository;
	
	public UsuarioCadastro adicionar(UsuarioCadastro usuariocadastro) {
		return repository.save(usuariocadastro);
	}
	
	public List<UsuarioCadastro> listarTodos() {
		return repository.findAll();
	}
	
	public UsuarioCadastro encontrarPorId(Long id) {
		return repository.findById(id)
		.orElseThrow(() -> new ResourceNotfoundException("Sem registro para esse ID."));
	}
	
	public UsuarioCadastro atualizarCadastro(UsuarioCadastro uc) {
		UsuarioCadastro entity = repository.findById(uc.getId())
				.orElseThrow(() -> new ResourceNotfoundException("Sem registro para esse ID."));
		entity.setNome(uc.getNome());
		entity.setEmail(uc.getEmail());
		entity.setSenha(uc.getSenha());
		return repository.save(entity);
	}

	public void delete(Long id) {
		UsuarioCadastro entity = repository.findById(id)
				.orElseThrow(() -> new ResourceNotfoundException("Sem registro para esse ID."));
		repository.delete(entity);
	}
	
}
